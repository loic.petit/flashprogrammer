// CONFIGURATION

#define TOTAL_DATA_PINS 16

byte address_pins[] = {
  9, 8, 7, 6, 5, 4, 3, 2,
  14, 15, 16, 17, 18, 19, 20, 21,
  10, 11
};

byte data_pins[] = {
  38, 42, 46, 50, 39, 43, 47, 51,
  40, 44, 48, 52, 41, 45, 49, 53
};

#define CE_PIN 13
#define OE_PIN 12
#define W_PIN 37
#define CHIP_STATUS_PIN 35
#define TOTAL_ADDRESS_PINS 18
#define ROM_SIZE 512*1024L


#define TOTAL_ADDRESSES (1L << TOTAL_ADDRESS_PINS)
// END ROM DEFINITION


void setup() {
  digitalWrite(OE_PIN, HIGH);
  digitalWrite(W_PIN, HIGH);
  digitalWrite(CE_PIN, HIGH);
  pinMode(OE_PIN, OUTPUT);
  pinMode(CE_PIN, OUTPUT);
  pinMode(W_PIN, OUTPUT);
  pinMode(CHIP_STATUS_PIN, INPUT);
  for (int i = 0; i < TOTAL_ADDRESS_PINS; i++) {
    pinMode(address_pins[i], OUTPUT);
  }
  for (int i = 0; i < TOTAL_DATA_PINS; i++) {
    pinMode(data_pins[i], INPUT);
  }
  Serial.begin(250000);
  delay(1000);
}

void inputMode() {
  for (int i = 0; i < TOTAL_DATA_PINS; i++) {
    pinMode(data_pins[i], INPUT);
  }
}

void outputMode() {
  for (int i = 0; i < TOTAL_DATA_PINS; i++) {
    pinMode(data_pins[i], OUTPUT);
    digitalWrite(data_pins[i], LOW);
  }
}

void setAddress(uint32_t value) {
  uint32_t address = 1;
  for (int i = 0; i < TOTAL_ADDRESS_PINS; i++) {
    digitalWrite(address_pins[i], (value & address) > 0);
    address = address << 1;
  }
}

uint16_t readBytes() {
  byte current = 0;
  uint16_t data = 0;
  delayMicroseconds(1);
  for (int i = TOTAL_DATA_PINS - 1; i >= 0; i--) {
    data = data << 1;
    current = digitalRead(data_pins[i]);
    data |= current & 1;
  }
  return data;
}

void setDataLine(uint16_t data) {
  uint16_t address = 1;
  for (int i = 0; i < TOTAL_DATA_PINS; i++) {
    digitalWrite(data_pins[i], (data & address) > 0);
    address = address << 1;
  }
}

void programBytes(uint32_t address, uint16_t data) {
  // write the 3 cycle
  setAddress(0x555);
  digitalWrite(W_PIN, LOW);
  setDataLine(0xAA);
  digitalWrite(W_PIN, HIGH);
  
  setAddress(0x2AA);
  digitalWrite(W_PIN, LOW);
  setDataLine(0x55);
  digitalWrite(W_PIN, HIGH);
  
  setAddress(0x555);
  digitalWrite(W_PIN, LOW);
  setDataLine(0xA0);
  digitalWrite(W_PIN, HIGH);
  
  setAddress(address);
  digitalWrite(W_PIN, LOW);
  setDataLine(data);
  digitalWrite(W_PIN, HIGH);

  // wait for the chip to be ready
  while(!digitalRead(CHIP_STATUS_PIN));
  
  return;
}
void resetChip() {
  
  outputMode();
  
  digitalWrite(CE_PIN, LOW);
  // write the 3 cycle
  setAddress(0x5555);
  digitalWrite(W_PIN, LOW);
  setDataLine(0xAA);
  digitalWrite(W_PIN, HIGH);
  
  setAddress(0x2AAA);
  digitalWrite(W_PIN, LOW);
  setDataLine(0x55);
  digitalWrite(W_PIN, HIGH);
  
  setAddress(0x5555);
  digitalWrite(W_PIN, LOW);
  setDataLine(0x80);
  digitalWrite(W_PIN, HIGH);
  
  setAddress(0x5555);
  digitalWrite(W_PIN, LOW);
  setDataLine(0xAA);
  digitalWrite(W_PIN, HIGH);

  setAddress(0x2AAA);
  digitalWrite(W_PIN, LOW);
  setDataLine(0x55);
  digitalWrite(W_PIN, HIGH);

  setAddress(0x5555);
  digitalWrite(W_PIN, LOW);
  setDataLine(0x10);
  digitalWrite(W_PIN, HIGH);
  
  inputMode();
  digitalWrite(CE_PIN, HIGH);
  
  return;
}


bool writeROM() {
  uint16_t data = 0;
  digitalWrite(CE_PIN, LOW);
  digitalWrite(OE_PIN, HIGH);
  outputMode();
  setAddress(0);
  delayMicroseconds(100);
  for (unsigned long i = 0; i < TOTAL_ADDRESSES; i++) {
#if TOTAL_DATA_PINS == 16
    while (Serial.available() < 2);
    data = Serial.read();
    data |= Serial.read() << 8;
#else
    while (Serial.available() == 0);
    data = Serial.read();
#endif
    programBytes(i, data);
    if ((i % 16) == 0) {
      // sync point
      Serial.write(1);
    }
  }
  digitalWrite(CE_PIN, HIGH);
  inputMode();
}

int readROM() {
  uint16_t data = 0;
  //read mode
  inputMode();
  digitalWrite(CE_PIN, LOW);
  digitalWrite(OE_PIN, LOW);
  for (unsigned long i = 0; i < TOTAL_ADDRESSES; i++) {
    setAddress(i);
    // impulse to make sure that the address is taken into account (27c4096 needs that)
    digitalWrite(OE_PIN, HIGH);
    digitalWrite(OE_PIN, LOW);
    data = readBytes();
    Serial.write(data & 255);
#if TOTAL_DATA_PINS == 16
    Serial.write(data >> 8);
#endif
  }
  setAddress(0);
  digitalWrite(OE_PIN, HIGH);
  digitalWrite(CE_PIN, HIGH);
}

byte in_byte = 0;
void loop() {
  long adr;
  unsigned long romsize = ROM_SIZE;
  unsigned long datapins = TOTAL_DATA_PINS;
  if (Serial.available()) {
    in_byte = Serial.read();
    if (in_byte == 0x55) {
      while (Serial.available() == 0);
      in_byte = Serial.read();
      switch (in_byte) {
        case 'w':
          writeROM();
          break;
        case 'r':
          readROM();
          break;
        case 'S':
          Serial.write((byte*) &romsize, sizeof(romsize));
          Serial.write((byte*) &datapins, sizeof(datapins));
          break;
        case 'p':
          adr = Serial.parseInt();
          outputMode();
          programBytes(0x20000, adr);
          inputMode();
          Serial.print("programmed bytes ");
          Serial.print(adr);
          Serial.print("\n");
          break;
        case 'O':
          digitalWrite(OE_PIN, HIGH);
          Serial.print("/OE is HIGH\n");
          break;
        case 'o':
          digitalWrite(OE_PIN, LOW);
          Serial.print("/OE is LOW\n");
          break;
        case 'E':
          digitalWrite(CE_PIN, HIGH);
          Serial.print("/CE is HIGH\n");
          break;
        case 'e':
          digitalWrite(CE_PIN, LOW);
          Serial.print("/CE is LOW\n");
          break;
        case 'X':
          digitalWrite(W_PIN, HIGH);
          Serial.print("/W is HIGH\n");
          break;
        case 'x':
          digitalWrite(W_PIN, LOW);
          Serial.print("/W is LOW\n");
          break;
        case 'Q':
          Serial.print("Resetting chip!");
          resetChip();
          Serial.print("\nDone\n");
        case 'c':
          Serial.print("Chip status: ");
          if (digitalRead(CHIP_STATUS_PIN)) {
            Serial.println("READY");
          } else {
            Serial.println("BUSY");
          }
          break;
        case 'n':
          adr = Serial.parseInt();
          setAddress(adr);
          Serial.print("Address is");
          Serial.print(adr);
          Serial.print("\n");
          break;
        case 'b':
          adr = readBytes();
          Serial.print("Read bytes are");
          Serial.print(adr);
          Serial.print("\n");
          break;
      }
    }
  }
}
