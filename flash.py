import io
import os

import serial
import time
import tqdm
import readline

print("WydD Flash Burner")

readline.set_completer_delims('\t')
readline.parse_and_bind("tab: complete")


def list_folder(path):
    """
    Lists folder contents
    """
    # absolute path
    basedir = os.path.dirname(path)
    contents = os.listdir(os.curdir if not basedir else basedir)
    # add back the parent
    contents = [os.path.join(basedir, d) for d in contents]
    return [e + (os.sep if os.path.isdir(e) else "") for e in contents]


def path_completer(text, state):
    return [x for x in list_folder(text) if x.startswith(text)][state]


ser = serial.Serial('COM10', 250000, timeout=0)
time.sleep(1)
ser.write(b"U")
ser.write(b"S")
while ser.inWaiting() < 8:
    pass
chipsize = int.from_bytes(ser.read(4), "little")
datapins = int.from_bytes(ser.read(4), "little")
print("Target flash capacity is: ", chipsize // 1024, "kB over ", datapins, "bits\n")

bytes_per_write = 2 if datapins == 16 else 1


def read_stream():
    ser.flushInput()
    ser.write(b"\x55")
    ser.write(bytes("r", "ASCII"))
    for _ in tqdm.trange(chipsize):
        while ser.inWaiting() == 0:
            time.sleep(0.1)
        yield ser.read(1)


while True:
    readline.set_completer()
    print("\tMenu")
    print("\t\t1- read")
    print("\t\t2- burn")
    print("\t\t3- blank check")
    print("\t\t4- verify")
    print("")
    print("\t\t7- quit\n")
    option = int(input("What do you want to do? "))

    if option == 1:
        readline.set_completer(path_completer)
        name = input("What the name of the file? ")
        with open(name, 'wb') as f:
            for byte in read_stream():
                f.write(byte)
        print("Done\n")
    if option == 2:
        readline.set_completer(path_completer)
        name = input("What's the name of the file? ")
        f = open(name, 'rb')
        all_bytes = f.read()
        f.close()
        if len(all_bytes) == chipsize:
            print("Correct size of ROM!")
        elif len(all_bytes) == chipsize // 2:
            print("Half sized ROM, writing the data twice")
            all_bytes = all_bytes + all_bytes
        elif len(all_bytes) == chipsize // 4:
            print("Quarter sized ROM, writing the data four times")
            all_bytes = all_bytes + all_bytes + all_bytes + all_bytes
        f = io.BytesIO(all_bytes)
        ser.flushInput()
        ser.write(b"U")
        ser.write(b"w")
        count_map = dict()
        for n in tqdm.trange(chipsize // bytes_per_write):
            data = f.read(bytes_per_write)
            ser.write(data)
            if (n % 16) == 0:
                # active wait to be as fast as possible
                while ser.inWaiting() == 0:
                    pass
                ser.read(1)
        f.close()
        print("\nDone")
    # Blank check
    if option == 3:
        blank = True
        for byte in read_stream():
            if byte[0] != 255:
                blank = False
                break
        if blank:
            print("\nThe chip is blank\n")
        else:
            print("\nThe chip seems to contain data\n")
    # This is for checking if the eprom was programmed right
    if option == 4:
        readline.set_completer(path_completer)
        name = input("What the name of the file?")

        with open(name, 'rb') as f:
            for n, byte in enumerate(read_stream()):
                reference = f.read(1)
                if byte[0] != reference[0]:
                    print("\nMEEEEEH BAD READ AT %x, read %x instead of %x" % (n, byte[0], reference[0]))

        print("Done\n")
    if option == 7:
        print("See ya!")
        break
    time.sleep(0.1)
